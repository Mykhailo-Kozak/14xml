<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">

        <html>
            <body>
                <h2>My devices</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Name</th>
                        <th>Origin</th>
                        <th>Price</th>
                    </tr>
                    <xsl:for-each select="devices/computer">
                        <xsl:sort select="name"/>
                        <xsl:if test="price &lt; 10"></xsl:if>
                        <tr>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="price"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>

    </xsl:template>

</xsl:stylesheet>