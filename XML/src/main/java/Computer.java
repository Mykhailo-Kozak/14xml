import java.util.ArrayList;
import java.util.List;

public class Computer {
    private int computerNumber;
    private String name;
    private String origin;
    private int price;
    private List<String> types = new ArrayList<String>();
    private boolean critcal;

    public Computer(int computerNumber, String name, String origin, int price, List<String> types, boolean critcal) {
        this.computerNumber = computerNumber;
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.types = types;
        this.critcal = critcal;
    }
}
